// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_CONSISTENT_EDGE_ORIENTATION_UNION_FIND_HH
#define DUNE_CONSISTENT_EDGE_ORIENTATION_UNION_FIND_HH

/**
 * @file This implements methods for a union and find data structure. These are the same methods as in boost,
 * except this passes maps by reference.
 */

template <class ParentMap, class RankMap, class T>
inline void make_set(ParentMap &p, RankMap &rank, const T& v){
  put(p, v, v);
  using R = typename RankMap::value_type;
  put(rank, v, R());
}

template <class ParentMap, class T>
T find(ParentMap& parent, T v)
{
  T old = v;
  T ancestor = get(parent, v);
  while (ancestor != v) {
    v = ancestor;
    ancestor = get(parent, v);
  }
  v = get(parent, old);
  while (ancestor != v) {
    put(parent, old, ancestor);
    old = v;
    v = get(parent, old);
  }
  return ancestor;
}

template <class ParentMap, class RankMap, class T>
inline void
union_sets(ParentMap &p, RankMap &rank, T i, T j)
{
  i = find(p, i);
  j = find(p, j);
  if (i == j) return;
  if (get(rank, i) > get(rank, j))
    put(p, j, i);
  else {
    put(p, i, j);
    if (get(rank, i) == get(rank, j))
      put(rank, j, get(rank, j) + 1);
  }
}

#endif //DUNE_CONSISTENT_EDGE_ORIENTATION_UNION_FIND_HH
