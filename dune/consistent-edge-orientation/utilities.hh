// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_CONSISTENT_EDGE_ORIENTATION_UTILITIES_HH
#define DUNE_CONSISTENT_EDGE_ORIENTATION_UTILITIES_HH

#include "utilities/edgedefinitions.hh"
#include "utilities/edgeaxis.hh"
#include "utilities/edgecontainer.hh"
#include "utilities/edgelocalgeometry.hh"
#include "utilities/edgeorientation.hh"

#endif //DUNE_CONSISTENT_EDGE_ORIENTATION_UTILITIES_HH
