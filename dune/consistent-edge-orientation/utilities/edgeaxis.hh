// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_CONSISTENT_EDGE_ORIENTATION_EDGEUTILITY_HH
#define DUNE_CONSISTENT_EDGE_ORIENTATION_EDGEUTILITY_HH

#include <array>
#include <dune/grid/common/rangegenerators.hh>
#include "edgedefinitions.hh"
#include "edgecontainer.hh"

namespace Dune {

  /**
   * @brief Determines for one axis of e if it needs to be flip to stay consistent with the global orientation
   * @tparam GV leaf grid view
   * @tparam Entity entity of codim 0
   */
  template<typename GV, typename Entity>
  bool axisNeedsFlip(const GV &gv, const Entity &e, const EdgeAxis axis, const OrientationMap<GV> &globalOrientation) {
    bool needsFlip = true;
    for (const auto edge: EdgeWrapper<GV>::generate_axis(gv, e, axis)) {
      needsFlip &= edgeOrientation(gv, edge) != globalOrientation[edge];
    }
#ifndef NDEBUG
    if (!needsFlip)
      for (const auto edge: EdgeWrapper<GV>::generate_axis(gv, e, axis))
        assert(edgeOrientation(gv, edge) == globalOrientation[edge]);
#endif
    return needsFlip;
  }

  /**
   * @brief Determines for all axis of e if they need to be flip to stay consistent with the global orientation
   * @tparam GV leaf grid view
   * @tparam Entity entity of codim 0
   */
  template<typename GV, typename Entity>
  std::array<bool, GV::Grid::dimension>
  axisNeedsFlip(const GV &gv, const Entity &e, const OrientationMap<GV> &globalOrientation) {
    constexpr int dim = GV::Grid::dimension;
    std::array<bool, dim> flip = {};

    for (const auto axis: Dune::range(dim))
      flip[axis] = axisNeedsFlip(gv, e, EdgeAxis(axis), globalOrientation);

    return flip;
  }

}
#endif //DUNE_CONSISTENT_EDGE_ORIENTATION_EDGEUTILITY_HH
