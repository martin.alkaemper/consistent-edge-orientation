// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_CONSISTENT_EDGE_ORIENTATION_EDGELOCALGEOMETRY_HH
#define DUNE_CONSISTENT_EDGE_ORIENTATION_EDGELOCALGEOMETRY_HH

#include <array>
#include <dune/grid/common/rangegenerators.hh>
#include <dune/common/reservedvector.hh>
#include <dune/common/power.hh>
#include "edgedefinitions.hh"

namespace Dune {

  template<typename GV>
  auto parallelEdgesInElement(const GV &gv, const EdgeWrapper<GV>& edge) {
    constexpr int dim = GV::Grid::dimension;

    const auto &idxSet = gv.indexSet();
    Dune::ReservedVector<Dune::EdgeWrapper<GV>, Dune::StaticPower<2, GV::Grid::dimension - 1>::power> parallelEdges;

    for (const auto parallelIndex: edge.parallelIndices()) {
      parallelEdges.push_back(EdgeWrapper<GV>(edge.element, parallelIndex,
                                              idxSet.subIndex(edge.element, parallelIndex, dim - 1)));
    }
    return parallelEdges;
  }
}


#endif //DUNE_CONSISTENT_EDGE_ORIENTATION_EDGELOCALGEOMETRY_HH
