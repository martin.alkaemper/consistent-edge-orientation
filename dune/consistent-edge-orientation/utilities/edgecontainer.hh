// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_CONSISTENT_EDGE_ORIENTATION_EDGECONTAINER_HH
#define DUNE_CONSISTENT_EDGE_ORIENTATION_EDGECONTAINER_HH

#include "edgedefinitions.hh"
#include "edgeorientation.hh"

namespace Dune{

  /**
   * @brief This is a vector like container, which can be accessed by Dune::EdgeWrapper. It is very similar to
   * boost::vector_property_map, except additional []-operator and it can be used in range based for loops.
   * @tparam T Value type of the container
   * @tparam GV Dune grid view
   */
  template<typename T, typename GV>
  class EdgeMap{
  public:
    using key_type = EdgeWrapper<GV>;
    using value_type = T;
    using reference = typename std::iterator_traits<typename std::vector<T>::iterator>::reference;

    constexpr static int dim = GV::Grid::dimension;
    using Edge = typename GV::template Codim<dim - 1>::Entity;

    explicit EdgeMap(const GV& gv_, const T& init = {})
        : storage(new std::vector<T>(gv_.size(dim - 1), init)), gv(gv_) {}


    reference operator[](const key_type& edgeWrapper) const {
      return (*storage)[edgeWrapper.globalIndex];
    }

    reference operator[](const std::size_t& i) const {
      return (*storage)[i];
    }

    typename std::vector<T>::iterator begin()
    {
      return storage->begin();
    }

    typename std::vector<T>::iterator end()
    {
      return storage->end();
    }

    typename std::vector<T>::const_iterator begin() const
    {
      return storage->begin();
    }

    typename std::vector<T>::const_iterator end() const
    {
      return storage->end();
    }

  private:
    std::shared_ptr<std::vector<T>> storage;
    GV gv;
  };


  template<typename GV>
  using RepresentativeMap = EdgeMap<EdgeWrapper<GV>, GV>;

  template<typename GV>
  using AdjacentElementsMap = EdgeMap<std::vector<EdgeWrapper<GV>>, GV>;

  template<typename GV>
  using OrientationMap = EdgeMap<EdgeOrientation, GV>;


  template<typename T, typename GV, typename K>
  typename EdgeMap<T, GV>::reference get(const EdgeMap<T, GV>& map, const K& k){
    return map[k];
  }

  template<typename T, typename GV, typename K>
  void put(const EdgeMap<T, GV>& map, const K& k, const T& v){
    map[k] = v;
  }

}
#endif //DUNE_CONSISTENT_EDGE_ORIENTATION_EDGECONTAINER_HH
