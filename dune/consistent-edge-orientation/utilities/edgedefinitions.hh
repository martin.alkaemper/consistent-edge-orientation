// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_CONSISTENT_EDGE_ORIENTATION_EDGEDEFINITIONS_HH
#define DUNE_CONSISTENT_EDGE_ORIENTATION_EDGEDEFINITIONS_HH

#include <array>

namespace Dune {


  /**
   * @brief Enum to distinguish edge axis. This is also used to access arrays.
   */
  enum EdgeAxis {
    x = 0, y = 1, z = 2
  };

  /**
   * @brief Positive orientation indicates that the global index of the first corner is less than the global index
   * of the second corner.
   */
  enum class EdgeOrientation {
    positive = 1,
    negative = -1
  };

  namespace impl{
    template<int d>
    struct GeometryInformation;
    template<>
    struct GeometryInformation<2>{
      static constexpr std::array<int, 2> corners[4] = {{0, 2}, {1, 3},
                                                        {0, 1}, {2, 3}};
      static constexpr std::array<int, 1> parallelEdges[4] = {{1}, {0},
                                                              {3}, {2}};
      static constexpr EdgeAxis localIndexToAxis[4] = {EdgeAxis::y, EdgeAxis::y, EdgeAxis::x, EdgeAxis::x};
      static constexpr std::array<int, 2> axisToLocalIndex[2] = {{2, 3}, {0,1}};
      static constexpr std::size_t edgesPerAxis = 2;
    };
    template<>
    struct GeometryInformation<3>{
      static constexpr std::array<int, 2> corners[12] = {{0, 4}, {1, 5}, {2, 6}, {3, 7},
                                                         {0, 2}, {1, 3}, {0, 1}, {2, 3},
                                                         {4, 6}, {5, 7}, {4, 5}, {6, 7}};
      static constexpr std::array<int, 3> parallelEdges[12] = {{1, 2,  3}, {0, 2,  3}, {0, 1,  3}, {0, 1,  2},
                                                               {5, 8,  9}, {4, 8,  9}, {7, 10, 11}, {6, 10, 11},
                                                               {4, 5,  9}, {4, 5,  8}, {6, 7,  11}, {6, 7,  10}};
      static constexpr EdgeAxis localIndexToAxis[12] = {EdgeAxis::z, EdgeAxis::z, EdgeAxis::z, EdgeAxis::z,
                                            EdgeAxis::y, EdgeAxis::y, EdgeAxis::x, EdgeAxis::x,
                                            EdgeAxis::y, EdgeAxis::y, EdgeAxis::x, EdgeAxis::x};
      static constexpr std::array<int, 4> axisToLocalIndex[3] = {{6, 7, 10, 11}, {4, 5, 8, 9}, {0, 1, 2, 3}};
      static constexpr std::size_t edgesPerAxis = 4;
    };

    constexpr std::array<int, 2> GeometryInformation<2>::corners[4];
    constexpr std::array<int, 1> GeometryInformation<2>::parallelEdges[4];
    constexpr EdgeAxis GeometryInformation<2>::localIndexToAxis[4];
    constexpr std::array<int, 2> GeometryInformation<2>::axisToLocalIndex[2];
    constexpr std::size_t GeometryInformation<2>::edgesPerAxis;

    constexpr std::array<int, 2> GeometryInformation<3>::corners[12];
    constexpr std::array<int, 3> GeometryInformation<3>::parallelEdges[12];
    constexpr EdgeAxis GeometryInformation<3>::localIndexToAxis[12];
    constexpr std::array<int, 4> GeometryInformation<3>::axisToLocalIndex[3];
    constexpr std::size_t GeometryInformation<3>::edgesPerAxis;
  }

  /**
   * @brief Collects necessary information about an edge.
   * @tparam GV Dune leaf grid view
   */
  template<typename GV>
  struct EdgeWrapper {
    using GeometryInfo = impl::GeometryInformation<GV::dimension>;
    using Element = typename GV::template Codim<0>::Entity;
    Element element;
    std::size_t localIndex;
    std::size_t globalIndex;

    EdgeWrapper() : element(), localIndex(), globalIndex() {};

    EdgeWrapper(const Element &entity_, const std::size_t localIdx, const std::size_t globalIdx)
        : element(entity_), localIndex(localIdx), globalIndex(globalIdx) {}

    bool operator==(const EdgeWrapper &other) const {
      return element == other.element and localIndex == other.localIndex;
    }

    bool operator!=(const EdgeWrapper &other) const {
      return element != other.element or localIndex != other.localIndex;
    }

    auto localCorners() const{
      return GeometryInfo::corners[localIndex];
    }

    auto parallelIndices() const{
      return GeometryInfo::parallelEdges[localIndex];
    }

    auto axis() const{
      return GeometryInfo::localIndexToAxis[localIndex];
    }

    /**
     * @brief Returns EdgeWrappers for all given localIndices.
     */
    template<std::size_t N>
    static std::array<EdgeWrapper, N>
    generate_n(const GV& gv, const Element& entity, const std::array<int, N> localIndices){
      constexpr int dim = GV::Grid::dimension;
      const auto &idxSet = gv.indexSet();

      std::array<EdgeWrapper<GV>, N> edgeWrappers;
      for (std::size_t i = 0; i < N; ++i)
        edgeWrappers[i] = EdgeWrapper<GV>(entity, localIndices[i], idxSet.subIndex(entity, localIndices[i], dim - 1));

      return edgeWrappers;
    }

    /**
     * @brief Returns EdgeWrappers for all edges parallel to one axis.
     */
    static std::array<EdgeWrapper, GeometryInfo::edgesPerAxis>
    generate_axis(const GV& gv, const Element& entity, const EdgeAxis axis){
      return generate_n(gv, entity, GeometryInfo::axisToLocalIndex[axis]);
    }
  };
}
#endif //DUNE_CONSISTENT_EDGE_ORIENTATION_EDGEDEFINITIONS_HH
