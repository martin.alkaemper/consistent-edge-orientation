// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_CONSISTENT_EDGE_ORIENTATION_EDGEORIENTATION_HH
#define DUNE_CONSISTENT_EDGE_ORIENTATION_EDGEORIENTATION_HH

#include <cassert>
#include <ostream>
#include "edgelocalgeometry.hh"

namespace Dune {

  std::ostream &operator<<(std::ostream &s, const EdgeOrientation o) {
    switch (o) {
      case EdgeOrientation::positive :
        s << "positive";
        break;
      case EdgeOrientation::negative :
        s << "negative";
        break;
    }
    return s;
  }

  template<typename GV>
  EdgeOrientation edgeOrientation(const GV &gv, const EdgeWrapper<GV> &edge) {
    constexpr int dim = GV::Grid::dimension;

    const auto &idxSet = gv.indexSet();
    const auto corners = edge.localCorners();

    if (idxSet.subIndex(edge.element, corners[0], dim) < idxSet.subIndex(edge.element, corners[1], dim))
      return EdgeOrientation::positive;
    else
      return EdgeOrientation::negative;
  }

  EdgeOrientation flipOrientation(const EdgeOrientation orientation) {
    switch (orientation) {
      case EdgeOrientation::positive:
        return EdgeOrientation::negative;
      case EdgeOrientation::negative:
        return EdgeOrientation::positive;
      default:
        throw std::runtime_error("Edge orientation must be either positive or negative");
    }
  }

  template<typename GV>
  bool flippedRelativeOrientation(const GV &gv, const EdgeWrapper<GV> &a, const EdgeWrapper<GV> &b) {
    assert(a.element == b.element);
    return edgeOrientation(gv, a) != edgeOrientation(gv, b);
  }
}
#endif //DUNE_CONSISTENT_EDGE_ORIENTATION_EDGEORIENTATION_HH
