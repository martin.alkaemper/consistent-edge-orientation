Consistent Edge Orientation
===========================

This module generates mesh files in the Gmsh format, for which two elements, sharing an edge, agree on the orientation
of the common edge. The papers [1][1] and [2][2] give an detailed description of the algorithm used here. The binary
`consistent-edge-orientation` generates a mesh file with consistent edge orientation directly from a given Gmsh file.

Requirements on the mesh
------------------------
The mesh can be oriented if it is one of the following types:
* A 2d orientable manifold.
* An extruded 2d mesh.
* A mesh resulting from subdividing tetrahedra into hexahedra.

If none of these cases apply, a consistent orientation may not exist. In that case, a consistent orientation can be found,
after uniformly refining the mesh.

Dependencies
------------
This module needs the following software:
* DUNE core libraries, (dune-common, dune-geometry, dune-grid) version 2.7-git.
* A c++14 compatible compiler (tested with g++-6).
* A DUNE grid module and its dependencies. The current version supports dune-uggrid and dune-alugrid, although
  other grids may be added. New grids must guarantee that for elements with positive volume the edge orientation
  given by a Gmsh file stays unchanged during the grid creation phase.

Install
-------
The [DUNE Homepage](https://dune-project.org) gives a full overview of the build and install process for DUNE modules.

[1]: https://doi.org/10.1145/3061708
[2]: https://doi.org/10.1137/15M1021325